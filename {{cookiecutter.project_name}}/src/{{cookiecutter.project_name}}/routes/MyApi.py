import logging

from flask import Blueprint

from ..exceptions.MyFlaskException import FlaskException
from ..misc.Response import ApiResponseSuccessHttpStatus
from ..misc.Response import make_response_body
from ..misc.logger import logger_register

logger_register.load_logger(new_logger={__name__: {'handlers': ['default', 'file'],
                                                   'level': 'INFO',
                                                   'propagate': False}})

logger = logging.getLogger(__name__)

my_api = Blueprint('first_api', __name__)

@my_api.route("/", methods=['GET'], endpoint="first api")
def first_api():
    import random
    _ = random.randint(1,50)
    if _%2 == 0:
        return make_response_body(payload=[],
                                  response_type=ApiResponseSuccessHttpStatus.request_success)
    else:
        raise FlaskException()