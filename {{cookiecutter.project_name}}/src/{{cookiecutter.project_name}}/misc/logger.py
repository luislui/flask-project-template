import logging
import logging.config
import os
from datetime import datetime

from flask import request
from pythonjsonlogger import jsonlogger


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        if not log_record.get('timestamp'):
            # this doesn't use record.created, so it is slightly off
            now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ')
            log_record['timestamp'] = now
        if log_record.get('level'):
            log_record['level'] = log_record['level'].upper()
        else:
            log_record['level'] = record.levelname
            
        log_record['current_route'] = request.url_rule
        log_record['requestId'] = request.environ.get("HTTP_X_REQUEST_ID")
        log_record['time'] = datetime.now().isoformat()


class LoggerRegister():
    def __init__(self,
                 store_file_path=None,
                 project_name="default_json_format_log",
                 rotation_time_unit=None,
                 rotation_time_interval=None,
                 rotation_backup_count=None):
        self.is_created = False
        self.default_logging_config = {
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'standard': {
                    '()': CustomJsonFormatter,
                    # 'format': '[%(asctime)s.%(msecs)06f][%(threadName)s:%(thread)d][%(errorTraceId)s][%(name)s:%(levelname)s:%(lineno)d)]\n[%(module)s:%(funcName)s]:%(message)s\n\n'
                    'format': '%(time)s %(requestId)s %(threadName)s:%(thread)d  %(name)s:%(levelname)s:%(lineno)d) %(module)s:%(funcName)s :%(message)s'
                    # 'class': 'pythonjsonlogger.jsonlogger.JsonFormatter',
                },
                'color': {
                    'class': "colorlog.ColoredFormatter",
                    'format': "%(log_color)s %(asctime)s - %(name)s - %(levelname)s - %(funcName)s(): - %(lineno)d: - %(message)s",
                    'datefmt': "%Y-%m-%d %H:%M:%S%Z"
                }
            },
            'handlers': {

                'file': {
                    'level': 'DEBUG',
                    'class': 'logging.handlers.TimedRotatingFileHandler',
                    'formatter': 'standard',
                    'filename': f'./log/{project_name}.log',
                    'when': 'h',
                    'interval': 24,
                    'backupCount': 100
                },

                'default': {
                    'level': 'INFO',
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler',
                    'stream': 'ext://sys.stdout',  # Default is stderr
                },

                'color': {
                    'level': 'INFO',
                    'formatter': 'color',
                    'class': 'logging.StreamHandler',
                    'stream': 'ext://sys.stdout',  # Default is stderr
                },
            },
            'loggers': {

            }
        }

        # if store_file_path == "./log" and not os.path.exists("log"):
        try:
            if store_file_path:
                os.makedirs(store_file_path)
            else:
                os.makedirs('log')
        except FileExistsError:
            logging.warning("log folder exists")
        if store_file_path:
            path_of_json_store = f"{store_file_path}/{project_name}.log"
            self.default_logging_config["handlers"]["file"]["filename"] = path_of_json_store
        else:
            self.default_logging_config["handlers"]["file"]["filename"].format(project_name)

        if rotation_time_unit and rotation_time_interval and rotation_backup_count:
            self.default_logging_config['handlers']['file']["when"] = rotation_time_unit
            self.default_logging_config['handlers']['file']["interval"] = rotation_time_unit
            self.default_logging_config['handlers']['file']["backupCount"] = rotation_time_unit

    def load_logger(self,
                    new_logger=None):
        """
        new_logger = {'__name__': {'handlers': ['default', 'file'],
                              'level': 'INFO',
                              'propagate': False}}
        """
        self.default_logging_config["loggers"] = {**self.default_logging_config["loggers"], **new_logger}

    def start_logging(self, dry_run=None):
        if not dry_run:
            logging.config.dictConfig(self.default_logging_config)
            self.is_created = True
        else:
            import pprint
            pprint.pprint(self.default_logging_config)


logger_register = LoggerRegister(project_name="{{cookiecutter.project_name}}")
