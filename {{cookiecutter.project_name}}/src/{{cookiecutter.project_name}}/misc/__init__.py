from sqlalchemy import inspect


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}


def object_as_str(obj, desired_col):
    for c in inspect(obj).mapper.column_attrs:
        if desired_col and c.key == desired_col:
            return getattr(obj, c.key)
