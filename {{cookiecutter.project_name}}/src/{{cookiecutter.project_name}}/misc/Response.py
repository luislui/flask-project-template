from typing import List, NamedTuple, Union, Optional, Dict

from flask import make_response, jsonify
from flask import request


class ApiResponseFailedHttpStatus(NamedTuple):
    data_not_found = 404
    bad_request = 400
    unknown_error = 555


class ApiResponseSuccessHttpStatus(NamedTuple):
    request_success = 200
    delete_success = 202
    update_success = 202
    create_success = 201


def make_response_body(response_type: Optional[Union[ApiResponseSuccessHttpStatus, ApiResponseFailedHttpStatus]],
                       error_message: str = "",
                       payload: Optional[Union[List, str, Dict[str, str]]] = []
                       ):
    """
    unify_response = {"success":bool,
                      "error":{"error_message":str},
                      "request_id":str,
                      "payload":None} # [{},{}] / ["str"] / [[],[],[]] / []

    :param error_trace_id:
    :param payload:
    :param response_type:
    :param error_message:
    :return:
    """
    if 399 >= response_type:
        success = True
    else:
        success = False
        payload = []

    response_body = {"success": success, 'payload': payload, 'error': {'error_message': error_message},
                     "request_id": request.environ.get("HTTP_X_REQUEST_ID")}
    return make_response(jsonify(response_body), response_type)


if __name__ == '__main__':
    pass
