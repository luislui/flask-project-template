from environs import Env

env = Env()
env.read_env('.env')


def raise_if(env_viron, wanted_type='str', exc=EnvironmentError, default=None):
    type_selector = {
        'int': env.int,
        'str': env.str
    }
    env_variable = type_selector[wanted_type](env_viron, None)
    if not env_variable:
        if default is not None:
            return default
        raise exc(f'{env_viron} variable not in your env')

    return env_variable


class POSTGRES():
    def __init__(self):
        self.user = raise_if("{{cookiecutter.project_name}}_PROGRESQL_USERNAME")
        self.pw = raise_if("{{cookiecutter.project_name}}_PROGRESQL_PASSWORD")
        self.db = raise_if("{{cookiecutter.project_name}}_PROGRESQL_DATABASE_NAME")
        self.host = raise_if("{{cookiecutter.project_name}}_PROGRESQL_HOST")
        self.port = raise_if("{{cookiecutter.project_name}}_PROGRESQL_PORT")


class Settings:
    def __init__(self):
        self.postgresql = None

    @property
    def get_postgresql(self):
        return POSTGRES().__dict__


settings = Settings()

if __name__ == '__main__':
    print(settings.get_postgresql)
