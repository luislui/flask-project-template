
from sqlalchemy.dialects.postgresql.base import UUID

from ..models import db


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(UUID, primary_key=True)
    first_name = db.Column(db.String(30))
    last_name = db.Column(db.String(70))
    email = db.Column(db.String(100), nullable=False, unique=True)
    avatar = db.Column(db.String(1000), server_default=db.text("NULL::character varying"))
    created_at = db.Column(db.DateTime(True), nullable=False)
    updated_at = db.Column(db.DateTime(True), nullable=False)
    deleted_at = db.Column(db.DateTime(True), index=True)
    version = db.Column(db.Integer, nullable=False, server_default=db.text("0"))
    active = db.Column(db.Boolean, nullable=False)
    tenant_id = db.Column(UUID,db.ForeignKey('tenants.id', ondelete='SET NULL', onupdate='CASCADE'), nullable=False, index=True)
    # api_key = db.Column(db.String(200))


