import logging
import sys
import traceback

from flask import Flask, request
from flask_request_id_header.middleware import RequestID
from werkzeug.exceptions import BadRequest

from Exceptions import BaseCustomeException
from misc.Response import ApiResponseSuccessHttpStatus
from misc.Response import make_response_body
from misc.config import settings
from misc.logger import logger_register
from models import db

logger = logging.getLogger(__name__)

logger_register.load_logger(new_logger={__name__: {'handlers': ['default', 'file'],
                                                   'level': 'INFO',
                                                   'propagate': False}})


def create_app():
    from routes.MyApi import my_api

    app = Flask(__name__)

    app.config["SQLALCHEMY_DATABASE_URI"] = f"postgresql+psycopg2://" \
                                            f"{settings.get_postgresql['user']}:{settings.get_postgresql['pw']}@" \
                                            f"{settings.get_postgresql['host']}:{settings.get_postgresql['port']}/" \
                                            f"{settings.get_postgresql['db']}"
    app.config["pool_pre_ping"] = True
    db.init_app(app)

    app.register_blueprint(my_api, url_prefix='/')

    # app.config['REDOC'] = {'spec_route': '/docs', 'title': '{{cookiecutter.project_name}}'}
    # _ = Redoc(app, '../docs/user_mgt.yml')

    def gen_log_record(error_msg):
        tb = sys.exc_info()[2]  # extract the current exception info
        exc_tup = traceback.extract_tb(tb)[-1]  # extract the deeper stack frame
        # manually build a LogRecord from that stack frame
        req_msg = request.__dict__
        req_msg.pop('headers', None)
        req_msg.pop('HTTP_CONNECTION', None)
        lr = logger.makeRecord(name=logger.name,
                               level=logging.ERROR,
                               fn=exc_tup[0],
                               lno=exc_tup[1],
                               msg=str(error_msg),
                               args={},
                               exc_info=None,
                               func=exc_tup[2],
                               extra={"request_info": req_msg,
                                      "stacktrace": traceback.format_exc()})
        return lr

    @app.errorhandler(Exception)
    def unhandled_exception(e):
        lr = gen_log_record(e)
        logger.handle(lr)
        return make_response_body(error_message="Unknown error: " + str(e),
                                  response_type=ApiResponseSuccessHttpStatus.unknown_error)

    @app.errorhandler(BaseCustomeException)
    def handle_exception(e):
        lr = gen_log_record(e)
        logger.handle(lr)
        response = e
        return make_response_body(error_message=response.description,
                                  response_type=response.code)

    @app.errorhandler(BadRequest)
    def bad_request_exception(e):
        lr = gen_log_record(e)
        logger.handle(lr)
        return make_response_body(
            error_message=str(e),
            response_type=ApiResponseSuccessHttpStatus.bad_request)

    return app


app = create_app()
logger_register.start_logging()
app.config['REQUEST_ID_UNIQUE_VALUE_PREFIX'] = 'UMID-'
# https://pypi.org/project/flask-request-id-header/
RequestID(app)


@app.route('/health')
def health():
    return make_response_body(payload=[],
                              response_type=ApiResponseSuccessHttpStatus.request_success)


if __name__ == '__main__':
    host = '{{cookiecutter.host}}'
    port = '{{cookiecutter.port}}'
    app.run(host=host, port=port)
