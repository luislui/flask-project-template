from ..exceptions import BaseCustomeException
from ..misc.Response import ApiResponseFailedHttpStatus


class FlaskException(BaseCustomeException):
    def __init__(self):
        self.description = f'flask error'
        self.code = ApiResponseFailedHttpStatus.unknown_error
