from werkzeug.exceptions import HTTPException


class BaseCustomeException(HTTPException):
    """Baseclass for all HTTP exceptions in User Management API.  This exception can be called as WSGI
    application to render a default error page or you can catch the subclasses
    of it independently and render nicer error messages.
    """


