from datetime import datetime

from ..models.UserModel import User


class UserDAO(User):

    def delete_record(self, user_id):
        existing_date = datetime.utcnow()

        _ = self.select_model(User).filter_by(id=user_id, deleted_at=None).get_model()
        if not _:
            return False
        for exists_data in _:
            self.update_for_filter_data(exists_data, deleted_at=existing_date)
        return True
