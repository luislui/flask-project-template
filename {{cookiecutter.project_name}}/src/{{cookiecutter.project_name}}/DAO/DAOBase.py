from typing import Dict, List

from ..misc import object_as_dict
from ..models import db


class DAOBase():
    def __init__(self):
        self.query_instance = None

    def select_model(self, *args):
        self.query_instance = db.session.query(*args)
        return self

    def select_model_attr(self, *args):
        self.query_instance = db.session.query(*args)
        return self

    def get_first(self):
        return self.query_instance.first()

    def create(self, model, **kwargs):
        new_record = model(**kwargs)
        db.session.add(new_record)
        db.session.flush()
        return new_record

    def update_or_create(self, model, **kwargs):
        if 'created_at' in kwargs:
            created_at = kwargs.pop('created_at')
        if 'updated_at' in kwargs:
            updated_at = kwargs.pop('updated_at')

        exist_data = db.session.query(model).filter_by(**kwargs).first()
        if exist_data:
            return False
        else:
            kwargs["updated_at"] = updated_at
            kwargs["created_at"] = created_at
            new_data = model(**kwargs)
            db.session.add(new_data)
            db.session.flush()
            # print(new_data.id)
            # return new_data.id
            # db.session.commit()
            # print(new_data.id)
            return new_data

    def filter_by(self, **kwargs):
        self.query_instance = self.query_instance.filter_by(**kwargs)
        return self

    def distinct(self):
        self.query_instance = self.query_instance.distinct()
        return self

    def filter(self, op, model1, model2):
        self.query_instance = self.query_instance.filter(op(model1, model2))
        return self

    def filter_by_list(self, filter_by_not=False, filter_func=None, filter_items=None):
        if filter_by_not:
            return self.query_instance.filter(~filter_func(filter_items))
        else:
            return self.query_instance.filter(filter_func(filter_items))
        return self

    def get_all(self) -> List[Dict[str, str]]:
        result = self.query_instance.all()
        if not result:
            return False
        return [r._asdict() for r in result]

    def get_limit(self, v):
        self.query_instance = self.query_instance.limit(v)
        return self

    def join(self, model):
        self.query_instance = self.query_instance.join(model)
        return self

    def get_row_data(self, model, filter_func=None, filter_items=None, filter_by_not_in=False, **kwargs):
        if filter_func and filter_items:
            if filter_by_not_in:
                exist_data = db.session.query(model).filter_by(**kwargs).filter(~filter_func(filter_items)).all()
            else:
                exist_data = db.session.query(model).filter_by(**kwargs).filter(filter_func(filter_items)).all()

        else:
            exist_data = db.session.query(model).filter_by(**kwargs).all()
        if not exist_data:
            return False
        return exist_data

    def delete_if_exists(self, model, **kwargs):
        exist_data = db.session.query(model).filter_by(**kwargs).first()
        if not exist_data:
            return False
        else:
            db.session.delete(exist_data)
            db.session.commit()
            return True

    def update_if_exists(self, model, id: Dict[str, str], **kwargs):
        exist_data = db.session.query(model).filter_by(**id).first()
        if not exist_data:
            return False
        else:
            for key, name in kwargs.items():
                setattr(exist_data, key, name)
            db.session.flush()
            return True

    def update_for_filter_data(self, model, **kwargs):
        for key, name in kwargs.items():
            setattr(model, key, name)
        db.session.flush()
        return True

    def get_model(self):
        return self.query_instance.all()

    def get_model_all(self, fields):
        res = []
        for query_obj in self.query_instance.all():
            data = object_as_dict(query_obj, fields)
            res.append(data)
        return res