## A Flask template

### This template provide a project handle for your project begin:

- Implemented an flask official exception handle 
- Traced allow your error or log in each request
- Built your custome json formatter logging
- Managed your exception in flask is more easier
- Implemented logging 
- Built API with Blueprint
- Provided Dockerfile with server health check
- Provided default Data Access Object(DAO) layer for ORM(flask_sqlalchemy)
- Set project configuration on env variable

### Project Structure
```
📦my-flask-project
 ┣ 📂src
 ┃ ┗ 📂my-flask-project
 ┃ ┃ ┣ 📂DAO
 ┃ ┃ ┃ ┣ 📜DAOBase.py
 ┃ ┃ ┃ ┣ 📜FirstDAO.py
 ┃ ┃ ┃ ┗ 📜__init__.py
 ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┣ 📜MyFlaskException.py
 ┃ ┃ ┃ ┗ 📜__init__.py
 ┃ ┃ ┣ 📂misc
 ┃ ┃ ┃ ┣ 📜Response.py
 ┃ ┃ ┃ ┣ 📜__init__.py
 ┃ ┃ ┃ ┣ 📜config.py
 ┃ ┃ ┃ ┗ 📜logger.py
 ┃ ┃ ┣ 📂models
 ┃ ┃ ┃ ┣ 📜Base.py
 ┃ ┃ ┃ ┣ 📜UserModel.py
 ┃ ┃ ┃ ┗ 📜__init__.py
 ┃ ┃ ┣ 📂routes
 ┃ ┃ ┃ ┣ 📜MyApi.py
 ┃ ┃ ┃ ┗ 📜__init__.py
 ┃ ┃ ┣ 📜.env
 ┃ ┃ ┗ 📜app.py
 ┣ 📜Dockerfile
 ┣ 📜README.md
 ┗ 📜requirements.txt
```

### Usage

1. install cookiecutter
    - `pip install cookiecutter`

2. build the template with cookiecutter
    - `cookiecutter git@gitlab.com:wilsom20012/flask-project-template.git`

3. input the project name, host and port
